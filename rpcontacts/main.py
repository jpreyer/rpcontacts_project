# -*- coding: utf-8 -*-

"""This module provides RP Contacts application."""

import sys
import hashlib, binascii, os

from PyQt5.QtWidgets import QApplication

from .database import createConnection
from .views import Window

def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')

def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',provided_password.encode('utf-8'),salt.encode('ascii'),100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password

def main():
    """Set password for the app"""
    stored_password = hash_password('thisisatest')

    """Prompt for user password"""
    entered_password = input("Enter password for application:")
    if (verify_password(stored_password, entered_password)):


        """RP Contacts main function."""
        # Create the application
        app = QApplication(sys.argv)
        # Connect to the database before creating any window
        if not createConnection("contacts.sqlite"):
            sys.exit(1)
        # Create the main window if the connection succeeded
        win = Window()
        win.show()
        # Run the event loop
        sys.exit(app.exec_())
    else:
        print ("Incorrect password entered")
        exit(1)
